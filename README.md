Manage wireguard peers, generate server config file, generate qr codes for peers to scan.

    1. Generate a private key for server.conf.tmpl
    2. generate a public key for client.conf.tmpl
    3. Add your enpoint info
    4. run this tool to add peers
    5. run "write" to output to wg99.conf

Default out is to /etc/wireguard/wg99.conf. Bring interface up with "systemctl start wg-quick@wg99"

Prerequisites:

    - wireguard https://www.wireguard.com/install/
    
    - qrencode dnf/apt install qrencode

Build:
    go build wgpeermanager.go
    
Run:
    ./wgpeermanager command option
    
    available commands:
        add: adds a new peer with the name specified, displays QR code
            eg. ./wgpeermanager add jim
        remove: removes the named peer, if multiple of the same exist, deletes both
            eg. ./wgpeermanager remove jim
        list: lists all of your added peers
            eg. ./wgpeermanager list
        write: generates your new config file to use
            eg. ./wgpeermanager write
        show: shows the QR code for an existing peer
            eg. ./wgpeermanager show jim