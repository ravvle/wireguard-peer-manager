package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"text/template"
	"strings"
)

type WGPeer struct {
	Number     int
	PublicKey  string
	PrivateKey string
	Name       string
}

var iprange = "192.168.2." //dont put the last number, code too dumb
var ip6range = "fd42:42:42::"

var peersfile = "peers.json"
var serverconfigfile = "server.conf.tmpl"
var clientconfigfile = "client.conf.tmpl"

var outputconfig = "/etc/wireguard/wg99.conf"

var peers []WGPeer

func loadPeers() {

	// read file
	data, err := ioutil.ReadFile(peersfile)
	check(err)

	// unmarshall it
	err2 := json.Unmarshal(data, &peers)
	check(err2)

}

func writePeers() {

	var jsonData []byte
	jsonData, err := json.Marshal(peers)
	check(err)

	err2 := ioutil.WriteFile(peersfile, jsonData, 0600)
	check(err2)

}

func writeWGConf() {

	data, err := ioutil.ReadFile(serverconfigfile)
	check(err)

	config := string(data) + "\n"

	for _, peer := range peers {
		config += "\n[Peer]\n"
		config += "PublicKey = " + peer.PublicKey + "\n"
		config += fmt.Sprintf("%s %s%d%s, %s%d%s\n", "AllowedIPs = ", iprange, peer.Number, "/32",ip6range, peer.Number, "/128")
	}

	err2 := ioutil.WriteFile(outputconfig, []byte(config), 0600)
	check(err2)

}

func clientConfig(p WGPeer) string {
	ut, err := template.ParseFiles(clientconfigfile)
	check(err)

	var b bytes.Buffer

	err2 := ut.Execute(&b, p)
	check(err2)
	return b.String()
}

//this doesn't work for some reason
func checkIfQRencode() {
	outp, _ := exec.Command("sh", "-c", "qrencode -V").Output()
	if strings.Compare(string(outp[0:16]), "qrencode version") != 0 {
		fmt.Println("You dont have qrencode installed, install it with \"dnf install qrencode\" or \"apt install qrencode\"")
		fmt.Println(string(outp[0:16]))
		os.Exit(1)
	}
}

func createQR(str string) []byte {
	cmd := `echo "` + str + `" > tempqrfile`
	_, err := exec.Command("sh", "-c", cmd).Output()
	check(err)
	outp, err2 := exec.Command("sh", "-c", "qrencode -t ansiutf8 < tempqrfile").Output()
	check(err2)
	_, err3 := exec.Command("sh", "-c", "rm -f tempqrfile").Output()
	check(err3)

	return outp
}

func check(e error) {
	if e != nil {
		fmt.Print(e)
	}
}

func nextNumber() int {
	if len(peers) <= 0 {
		return 2
	} else {
		highest := 0
		for _, peer := range peers {
			if peer.Number > highest {
				highest = peer.Number
			}
		}
		return highest + 1
	}
}

func main() {

	//checkIfQRencode()

	loadPeers()

	arg := os.Args[1]

	switch arg {
	case "add":
		fmt.Println("add")
		name := os.Args[2]

		privkey, err := exec.Command("sh", "-c", "wg genkey > tempprivkey && cat tempprivkey").Output()
		check(err)
		pubkey, err2 := exec.Command("sh", "-c", "wg pubkey < tempprivkey").Output()
		check(err2)
		_, err3 := exec.Command("sh", "-c", "rm -f tempprivkey").Output()
		check(err3)

		newPeer := WGPeer{
			Number:     nextNumber(),
			PublicKey:  string(pubkey[:len(pubkey)-1]),
			PrivateKey: string(privkey[:len(privkey)-1]), //need to do -1 to remove the newline character
			Name:       name,
		}

		peers = append(peers, newPeer)

		writePeers()

		fmt.Println("added", name)

		config := clientConfig(newPeer)

		qr := createQR(config)

		fmt.Println(string(qr))

	case "remove":
		fmt.Println("remove")
		name := os.Args[2]

		var keepingpeers []WGPeer

		for _, peer := range peers {
			if peer.Name != name {
				keepingpeers = append(keepingpeers, peer)
			}
		}

		peers = keepingpeers

		writePeers()
	case "list":
		fmt.Println("list")
		for _, peer := range peers {
			fmt.Println(peer.Number, peer.Name, peer.PublicKey, peer.PrivateKey)
		}
	case "write":
		fmt.Println("write")
		writeWGConf()
	case "show":
		fmt.Println("show")
		name := os.Args[2]

		for _, peer := range peers {
			if peer.Name == name {
				fmt.Println(peer.Number, peer.Name)

				config := clientConfig(peer)
				qr := createQR(config)

				fmt.Println(string(qr), "\n")
			}
		}
	default:
		fmt.Println("idk what that command is")
	}

}
